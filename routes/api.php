<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/** Kargolar */
use App\Http\Controllers\YurticiKargoController;
use App\Http\Controllers\ArasKargoController;

/** SMS */
use App\Http\Controllers\NetGSMController;


/** Pazaryerleri */
use App\Http\Controllers\N11Controller;
use App\Http\Controllers\PaynetController;
use App\Http\Controllers\TrendyolController;

/** Ödeme Sistemleri */
use App\Http\Controllers\IParaController;


Route::prefix('/aras')->group(function () {
    Route::get('/customer-reference/{customerId}', [ArasKargoController::class, 'customerReference']);
    Route::get('/date-cargo/{date}', [ArasKargoController::class, 'dateCargo']);
    Route::get('/due-date-cargo/{date}', [ArasKargoController::class, 'dueDateCargo']);
    Route::get('/due-date-irsaliye/{date}', [ArasKargoController::class, 'dueDateIrsaliye']);
    Route::get('/waiting-cargo', [ArasKargoController::class, 'waitingCargo']);
    Route::get('/directed-cargo/{date}', [ArasKargoController::class, 'directedCargo']);
    Route::get('/sent-back-cargo/{date}', [ArasKargoController::class, 'sentBackCargo']);
    Route::get('/moving-info-cargo/{cargoFollowingCode}', [ArasKargoController::class, 'movingInfoCargo']);
    Route::get('/all-store-data', [ArasKargoController::class, 'allStoreData']);
    Route::get('/customer-reference-list/{customerIds}', [ArasKargoController::class, 'customerReferenceList']);
    Route::get('/interval-irsaliye/{startDate}/{endDate}', [ArasKargoController::class, 'intervalIrsaliye']);
    Route::get('/interval-irsaliye-with-payment/{startDate}/{endDate}', [ArasKargoController::class, 'intervalIrsaliyeWithPayment']);
    Route::get('/cargo-detail/{startDate}/{endDate}', [ArasKargoController::class, 'intervalIrsaliyeWithPayment']);
    Route::get('/cargo-between-date/{startDate}/{endDate}', [ArasKargoController::class, 'cargoBetweenDate']);
    Route::get('/invoice-info/{reportType}/{serialNumber}', [ArasKargoController::class, 'invoiceInfo']);
    Route::get('/cargo-with-barcode/{barcodeNumber}', [ArasKargoController::class, 'invoiceInfo']);
    Route::get('/cargo/barcode/{barcode}', [ArasKargoController::class, 'cargoBarcode']);
    Route::get('/get-cargo/{trackingNumber}', [ArasKargoController::class, 'getCargoInformation']);
});

 
Route::prefix('/yurtici')->group(function(){
    Route::post('/{type}/create-cargo', [YurticiKargoController::class, 'createCargo']);
    Route::post('/{type}/cancel-cargo', [YurticiKargoController::class, 'cancelCargo']);
    Route::post('/{type}/cargo-status', [YurticiKargoController::class, 'cargoStatus']);
});


// Route::prefix('/parasut')->group(function(){

// });

Route::prefix('/n11')->group(function(){
    Route::get('/get-cities', [N11Controller::class, 'getCities']);
    Route::get('/get-categories', [N11Controller::class, 'getCategories']);
    Route::post('/get-product-list/{itemsPerPage}/{currentPage}', [N11Controller::class, 'getProductList']);
    Route::get('/get-product/{productSellerCode}', [N11Controller::class, 'getProductBySeller']);
    Route::post('/save-product', [N11Controller::class, 'saveProduct']);
    Route::post('/delete-product-by-seller/{productSellerCode}', [N11Controller::class, 'deleteProductBySeller']);
    Route::post('/order-list', [N11Controller::class, 'orderList']);
    Route::post('/get-shipment-template', [N11Controller::class, 'getShipmentTemplate']);
    Route::get('/get-shipment-companies', [N11Controller::class, 'getShipmentCompanies']);
});



Route::prefix('/netgsm')->group(function(){
    Route::get('/account-detail', [NetGSMController::class, 'accountDetail']);
    Route::get('/send-sms', [NetGSMController::class, 'sendSms']);
    Route::get('/send-otp', [NetGSMController::class, 'sendOtp']);
});

Route::prefix('trendyol')->group(function(){

    Route::prefix('products')->group(function(){
        Route::get('/{page}/{size}', [TrendyolController::class, 'getProducts']);
        Route::post('/transfer', [TrendyolController::class, 'transferProducts']);
        Route::put('/update', [TrendyolController::class, 'updateProducts']);
        Route::get('/batch-request/{id}', [TrendyolController::class, 'checkBatchRequest']);

        Route::get('/product-categories', [TrendyolController::class, 'productCategories']);
        Route::get('/product-categories/{categoryId}/attributes', [TrendyolController::class, 'productCategoriesAttributes']);

        Route::post('/price-and-inventory', [TrendyolController::class, 'updatePriceAndInventory']);
    });


    Route::prefix('orders')->group(function(){
        Route::get('/{orderByField}/{orderByDirection}/{size}', [TrendyolController::class, 'getOrders']);
        
        Route::put('/{shipmentPackageId}/update-tracking-number', [TrendyolController::class, 'updateTrackingNumber']);
        Route::put('/shipment-packages/{shipmentPackageId}', [TrendyolController::class, 'updatePackageStatus']);
        Route::post('/supplier-invoice-links', [TrendyolController::class, 'setSupplierInvoiceLink']);
        Route::post('/shipment-packages/{shipmentPackageId}/cargo-providers', [TrendyolController::class, 'changeCargoProviders']);
    }); 


    // Route::prefix('claims')->group(function(){
    //     Route::get('/', [TrendyolController::class, 'getClaims']);
    //     Route::post('/create', [TrendyolController::class, 'createClaim']);
    //     Route::put('/{claimId}/items/approve', [TrendyolController::class, 'approveClaim']);
    //     Route::post('/issue/{claimIssueReasonId}/{claimItemIdList}/{description}', [TrendyolController::class, 'createRejection']);
    // });


    Route::prefix('questions')->group(function(){
        Route::get('/', [TrendyolController::class, 'getCustomerQuestions']);
        Route::post('/{questionId}/answer', [TrendyolController::class, 'answerCustomerQuestions']);
    });

    Route::get('/brands', [TrendyolController::class, 'getBrands']);
    Route::get('/shipment-providers', [TrendyolController::class, 'getShipmentProviders']);

    Route::get('/addresses', [TrendyolController::class, 'getAddresses']);


});

Route::prefix('/ipara')->group(function(){
    Route::post('/add-cart-to-wallet', [IParaController::class, 'addCartToWallet']);
    Route::post('/delete-card-from-wallet', [IParaController::class, 'deleteCardFromWallet']);
    Route::post('/get-card-from-wallet', [IParaController::class, 'getCardFromWallet']);

    Route::prefix('payment')->group(function(){
        Route::post('/3d', [IParaController::class, 'payment3D']);
        Route::post('/link', [IParaController::class, 'paymentLink']);
        
    });
});


Route::prefix('/paynet')->group(function(){
    Route::get('/', [PaynetController::class, 'index']);
});