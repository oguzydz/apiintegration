<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class TrendyolService
{
    /** Trendyol Auth Requirements */
    protected $supplierId;
    protected $username;
    protected $password;
    protected $token;

    /** API Url */
    public $apiUrl;

    public function __construct()
    {
        $this->supplierId = '107524';
        $this->username = 'DPrAEwBfkMDvgjuMuaTL';
        $this->password = 'yFA3VWPhQ4s9mNSxWOud';
        $this->apiUrl = 'https://api.trendyol.com/sapigw';


    }

    public function getProducts($page = null, $size = null)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/products?page='.$page.'&size='. $size;

        $response = Http::withBasicAuth($this->username, $this->password)->get($url);
        return $response;
    }

    public function transferProducts($data)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/v2/products';
      
        $response = Http::withBasicAuth($this->username, $this->password)->post($url, $data);
        return $response;
    }
 
    public function updateProducts($data)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/v2/products';
      
        $response = Http::withBasicAuth($this->username, $this->password)->put($url, $data);
        return $response;
    }

    public function checkBatchRequest($id)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/products/batch-requests/'.$id;
      
        $response = Http::withBasicAuth($this->username, $this->password)->get($url);
        return $response;
    }

    public function productCategories()
    {

        $url =  $this->apiUrl .'/product-categories';
      
        $response = Http::get($url);
        return $response;
    }


    public function productCategoriesAttributes($id)
    {

        $url =  $this->apiUrl .'/product-categories/'.$id.'/attributes';
      
        $response = Http::get($url);
        return $response;
    }

    public function getBrands()
    {

        $url =  $this->apiUrl .'/brands';
      
        $response = Http::get($url);
        return $response;
    }

    public function getShipmentProviders()
    {

        $url =  $this->apiUrl .'/shipment-providers';
      
        $response = Http::get($url);
        return $response;
    }

    public function getAddresses()
    {

        $url =  $this->apiUrl .'/suppliers/'. $this->supplierId . '/addresses';
      
        $response = Http::withBasicAuth($this->username, $this->password)->get($url);
        return $response;
    }

    public function updatePriceAndInventory($data)
    {

        $url =  $this->apiUrl .'/suppliers/'. $this->supplierId . '/products/price-and-inventory';
      
        $response = Http::withBasicAuth($this->username, $this->password)->post($url, $data);
        return $response;
    }





    public function getOrders($orderByField, $orderByDirection, $size){

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/orders?orderByField='.$orderByField.'&orderByDirection='. $orderByDirection.'&size='. $size;

        $response = Http::withBasicAuth($this->username, $this->password)->get($url);
        return $response;
    }


    public function updateTrackingNumber($shipmentPackageId, $trackingNumber)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/' . $shipmentPackageId . '/update-tracking-number';
      
        $response = Http::withBasicAuth($this->username, $this->password)->put($url, [
            'trackingNumber' => $trackingNumber
        ]);
        return $response;
    }


    public function updatePackageStatus($shipmentPackageId, $data)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/shipment-packages/' . $shipmentPackageId;
        
        $response = Http::withBasicAuth($this->username, $this->password)->put($url, $data);
        return $response;
    }

    public function setSupplierInvoiceLink($data)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/supplier-invoice-links';
        
        $response = Http::withBasicAuth($this->username, $this->password)->post($url, $data);
        return $response;
    }
    
    
    public function changeCargoProviders($id, $data)
    {

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/shipment-packages/'.$id.'/cargo-providers';
        
        $response = Http::withBasicAuth($this->username, $this->password)->post($url, $data);
        return $response;
    }



    public function getClaims(){

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/claims';

        $response = Http::withBasicAuth($this->username, $this->password)->get($url);
        return $response;
    }

    public function createClaim($data){

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/claims/create';

        $response = Http::withBasicAuth($this->username, $this->password)->post($url, $data);
        return $response;
    }


    public function approveClaim($claimId, $data){

        $url =  $this->apiUrl .'/suppliers/claims/'.$claimId.'/items/approve';

        $response = Http::withBasicAuth($this->username, $this->password)->put($url, $data);
        return $response;
    }

    public function createRejection($claimIssueReasonId, $claimItemIdList, $description){

        $url =  $this->apiUrl .'/suppliers/claims/'.$claimId.'/items/issue?claimIssueReasonId='. $claimIssueReasonId . '&claimItemIdList=' . $claimItemIdList . '&description=' . $description ;

        $response = Http::withBasicAuth($this->username, $this->password)->post($url);
        return $response;
    }




    public function getCustomerQuestions(){

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/questions/filter';

        $response = Http::withBasicAuth($this->username, $this->password)->get($url);
        return $response;
    }

    
    public function answerCustomerQuestions($questionId, $data){

        $url =  $this->apiUrl .'/suppliers/' . $this->supplierId  . '/questions/'. $questionId .'/answers';

        $response = Http::withBasicAuth($this->username, $this->password)->post($url, $data);
        return $response;
    }
}
