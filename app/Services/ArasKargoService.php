<?php

namespace App\Services;

use SoapClient;
use PharIo\Manifest\InvalidUrlException;

class ArasKargoService
{
    private $username;
    private $password;
    private $customerCode;

    public function __construct()
    {
        $this->username = 'info@hobidunya.com';
        $this->password = 'HD477312';
        $this->customerCode = '1505641931206';
    }

    public function ArasClient($queryInfo)
    {
        $loginInfo =
            '<LoginInfo><UserName>' .
            $this->username .
            '</UserName><Password>' .
            $this->password .
            '</Password><CustomerCode>' .
            $this->customerCode .
            '</CustomerCode></LoginInfo>';

        try {
            $client = new SoapClient(
                'http://customerservices.araskargo.com.tr/ArasCargoCustomerIntegrationService/ArasCargoIntegrationService.svc?wsdl'
            );

            $result = $client->GetQueryJSON([
                'loginInfo' => $loginInfo,
                'queryInfo' => $queryInfo,
            ]);
        } catch (InvalidUrlException $e) {
            return $e;
        }

        return response($result->GetQueryJSONResult, 200);
    }
}
