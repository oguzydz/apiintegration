<?php

namespace App\Services;
use Exception;
use SoapClient;

class N11Service
{
    protected static $_appKey, $_appSecret, $_parameters, $_sclient;
    public $_debug = false;

    public function __construct(array $attributes = [])
    {
        self::$_appKey = $attributes['appKey'];
        self::$_appSecret = $attributes['appSecret'];
        self::$_parameters = [
            'auth' => [
                'appKey' => self::$_appKey,
                'appSecret' => self::$_appSecret,
            ],
        ];
    }

    public function setUrl($url)
    {
        self::$_sclient = new SoapClient($url);
    }

    public function GetTopLevelCategories()
    {
        try {
            $this->setUrl('https://api.n11.com/ws/CategoryService.wsdl');
            return [self::$_sclient->GetTopLevelCategories(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function GetCities()
    {
        try {
            $this->setUrl('https://api.n11.com/ws/CityService.wsdl');
            return [self::$_sclient->GetCities(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function GetProductList($itemsPerPage, $currentPage)
    {
        try {
            $this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
            self::$_parameters['pagingData'] = [
                'itemsPerPage' => $itemsPerPage,
                'currentPage' => $currentPage,
            ];
            return [self::$_sclient->GetProductList(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function GetProductBySellerCode($sellerCode)
    {
        try {
            $this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
            self::$_parameters['sellerCode'] = $sellerCode;
            return [
                self::$_sclient->GetProductBySellerCode(self::$_parameters),
            ];
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function SaveProduct(array $product = [])
    {
        try {
            $this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
            self::$_parameters['product'] = $product;
            return [self::$_sclient->SaveProduct(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function DeleteProductBySellerCode($productSellerCode)
    {
        try {
            $this->setUrl('https://api.n11.com/ws/ProductService.wsdl');
            self::$_parameters['productSellerCode'] = $productSellerCode;
            return [self::$_sclient->DeleteProductBySellerCode(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }

    }

    public function OrderList($searchData)
    {
        try {
            $this->setUrl('https://api.n11.com/ws/OrderService.wsdl');
            self::$_parameters['searchData'] = $searchData;
            return [self::$_sclient->OrderList(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }

    }

    public function GetShipmentTemplate($name)
    {
        try {
            $this->setUrl('https://api.n11.com/ws/ShipmentService.wsdl');
            self::$_parameters['name'] = $name;
            return [self::$_sclient->GetShipmentTemplate(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }
 
    }

    public function GetShipmentCompanies()
    {
        try {
            $this->setUrl('https://api.n11.com/ws/ShipmentCompanyService.wsdl');
            return [self::$_sclient->GetShipmentCompanies(self::$_parameters)];
        } catch (\Exception $e) {
            return [$e];
        }
 
    }

    public function __destruct()
    {
        if ($this->_debug) {
            return [self::$_parameters];
        }
    }
}
