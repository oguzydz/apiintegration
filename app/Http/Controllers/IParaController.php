<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Spatie\ArrayToXml\ArrayToXml;

class IParaController extends Controller
{
    public $PublicKey;
    public $PrivateKey;
    public $BaseUrl;
    public $Mode;
    public $Version;
    public $HashString;
    public $transactionDate;

    public function __construct()
    {
        date_default_timezone_set('Europe/Istanbul');
        $this->transactionDate = date('Y-m-d H:i:s');
        $this->PublicKey = 'N8N5HHODTJ611Z7';
        $this->PrivateKey = 'N8N5HHODTJ611Z7EC7MLLUDWN';
        $this->BaseUrl = 'https://api.ipara.com/';
        $this->Version = '1.0';
        $this->Mode = 'T'; // Test -> P,
        $this->HashStringToken = 'N8N5HHODTJ611Z7:jg9FqYSJtfduV6BjyOhSavw+1Hs=';
        $this->token = $this->CreateToken($this->PublicKey, $this->HashString);
        $this->GUID = $this->GUID();
        $this->get_client_ip = $this->get_client_ip();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCartToWallet(Request $request)
    {
        $this->HashString =
            $this->PrivateKey .
            $request->userId .
            $request->cardOwnerName .
            $request->cardNumber .
            $request->cardExpireMonth .
            $request->cardExpireYear .
            $request->clientIp .
            $this->transactionDate;

        try {
            $response = Http::withHeaders($this->getHeader())->post(
                $this->BaseUrl . '/bankcard/create',
                $request->all()
            );

            return $response;
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function getCardFromWallet(Request $request)
    {
        $this->HashString =
            $this->PrivateKey .
            $request->userId .
            $request->cardId .
            $request->clientIp .
            $this->transactionDate;

        try {
            $response = Http::withHeaders($this->getHeader())->post(
                $this->BaseUrl . '/bankcard/inquiry',
                $request->all()
            );

            return $response;
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function deleteCardFromWallet(Request $request)
    {
        $this->HashString =
            $this->PrivateKey .
            $request->userId .
            $request->cardId .
            $request->clientIp .
            $this->transactionDate;

        try {
            $response = Http::withHeaders($this->getHeader())->post(
                $this->BaseUrl . '/bankcard/delete',
                $request->all()
            );

            return $response;
        } catch (\Exception $e) {
            return [$e];
        }
    }

    public function paymentLink(Request $request)
    {
        $this->HashString =
            $this->PrivateKey .
            $request->name .
            $request->surname .
            $request->email .
            $request->amount .
            $request->clientIp .
            $this->transactionDate;

        $header = [
            'Accept' => 'application/json',
            'Content-type' => 'application/json',
            'version' => $this->Version,
            'token' => $this->createToken($this->PublicKey, $this->HashString),
            'transactionDate' => $this->transactionDate,
        ];

        $request->expireDate = $this->transactionDate;

        $result = ArrayToXml::convert($request->all(), 'auth');



        try {
            $response = Http::withHeaders($header)->post(
                $this->BaseUrl . '/corporate/merchant/linkpayment/create',
                ['body' => $result]
            );

            return $response;
        } catch (\Exception $e) {
            return [$e];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment3D(Request $httpRequest)
    {
        // $this->HashString =
        //     $this->PrivateKey . $this->Mode . $this->transactionDate;

        // foreach ($request->all() as $value) {
        //     $this->HashString .= $value;
        // }

        $request = (object) [];
        $request->OrderId = $this->GUID();
        $request->Echo = 'Echo';
        $request->language = 'TR';
        $request->Mode = $this->Mode;
        $request->Amount = '10000'; // 100 tL
        $request->CardOwnerName = $httpRequest->nameSurname;
        $request->UserId = $httpRequest->userId;
        $request->CardId = $httpRequest->cardId;
        $request->CardNumber = $httpRequest->cardNumber;
        $request->CardExpireMonth = $httpRequest->month;
        $request->CardExpireYear = $httpRequest->year;
        $request->Installment = $httpRequest->installment;
        $request->Cvc = $httpRequest->cvc;
        $request->SuccessUrl = $httpRequest->successUrl;
        $request->FailUrl = $httpRequest->failUrl;

        // region Sipariş veren bilgileri
        $request->Purchaser = (object) [];
        $request->Purchaser->Name = 'Murat';
        $request->Purchaser->SurName = 'Kaya';
        $request->Purchaser->BirthDate = '1986-07-11';
        $request->Purchaser->Email = 'murat@kaya.com';
        $request->Purchaser->GsmPhone = '5881231212';
        $request->Purchaser->IdentityNumber = '1234567890';
        $request->Purchaser->ClientIp = $httpRequest->clientIp;
        // endregion

        // region Fatura bilgileri
        $request->Purchaser->InvoiceAddress = (object) [];
        $request->Purchaser->InvoiceAddress->Name = 'Murat';
        $request->Purchaser->InvoiceAddress->SurName = 'Kaya';
        $request->Purchaser->InvoiceAddress->Address =
            'Mevlüt Pehlivan Mah-> Multinet Plaza Şişli';
        $request->Purchaser->InvoiceAddress->ZipCode = '34782';
        $request->Purchaser->InvoiceAddress->CityCode = '34';
        $request->Purchaser->InvoiceAddress->IdentityNumber = '1234567890';
        $request->Purchaser->InvoiceAddress->CountryCode = 'TR';
        $request->Purchaser->InvoiceAddress->TaxNumber = '123456';
        $request->Purchaser->InvoiceAddress->TaxOffice = 'Kozyatağı';
        $request->Purchaser->InvoiceAddress->CompanyName = 'iPara';
        $request->Purchaser->InvoiceAddress->PhoneNumber = '2122222222';
        // endregion

        // region Kargo Adresi bilgileri
        $request->Purchaser->ShippingAddress = (object) [];
        $request->Purchaser->ShippingAddress->Name = 'Murat';
        $request->Purchaser->ShippingAddress->SurName = 'Kaya';
        $request->Purchaser->ShippingAddress->Address =
            'Mevlüt Pehlivan Mah-> Multinet Plaza Şişli';
        $request->Purchaser->ShippingAddress->ZipCode = '34782';
        $request->Purchaser->ShippingAddress->CityCode = '34';
        $request->Purchaser->ShippingAddress->IdentityNumber = '1234567890';
        $request->Purchaser->ShippingAddress->CountryCode = 'TR';
        $request->Purchaser->ShippingAddress->PhoneNumber = '2122222222';
        // endregion

        // region Ürün bilgileri
        $request->Products = [];
        $p = (object) [];
        $p->Title = 'Telefon';
        $p->Code = 'TLF0001';
        $p->Price = '5000';
        $p->Quantity = 1;
        $request->Products[0] = $p;

        $p = (object) [];
        $p->Title = 'Bilgisayar';
        $p->Code = 'BLG0001';
        $p->Price = '5000';
        $p->Quantity = 1;
        $request->Products[1] = $p;

        $this->HashString =
            $this->PrivateKey .
            $request->OrderId .
            $request->Amount .
            $request->Mode .
            $request->CardOwnerName .
            $request->CardNumber .
            $request->CardExpireMonth .
            $request->CardExpireYear .
            $request->Cvc .
            $request->UserId .
            $request->CardId .
            $request->Purchaser->Name .
            $request->Purchaser->SurName .
            $request->Purchaser->Email .
            $this->transactionDate;

        $header = [
            'Accept' => 'application/json',
            'Content-type' => 'application/json',
            'version' => $this->Version,
            'token' => $this->createToken($this->PublicKey, $this->HashString),
            'transactionDate' => $this->transactionDate,
        ];

        $paymentRequest = [
            'mode' => $this->Mode,
            'orderId' => $request->OrderId,
            'cardOwnerName' => $request->CardOwnerName,
            'cardNumber' => $request->CardNumber,
            'cardExpireMonth' => $request->CardExpireMonth,
            'cardExpireYear' => $request->CardExpireYear,
            'cardCvc' => $request->Cvc,
            'userId' => $request->UserId,
            'cardId' => $request->CardId,
            'installment' => $request->Installment,
            'amount' => $request->Amount,
            'echo' => $request->Echo,
            'successUrl' => $request->SuccessUrl,
            'failureUrl' => $request->FailUrl,
            'version' => $this->Version,
            'language' => $request->language,
            'purchaser' => $request->Purchaser,
            'products' => $request->Products,
        ];

        try {
            $response = Http::withHeaders($header)->post(
                'https://api.ipara.com/rest/payment/threed',
                $paymentRequest
            );

            return $response;
        } catch (\Exception $e) {
            return ['response' => $e];
        }
    }

    public function getHeader()
    {
        $header = [
            'Accept' => 'application/json',
            'Content-type' => 'application/json',
            'version' => $this->Version,
            'token' => $this->createToken($this->PublicKey, $this->HashString),
            'transactionDate' => $this->transactionDate,
        ];

        return $header;
    }

    public function CreateToken($publicKey, $hashString)
    {
        return $publicKey . ':' . base64_encode(sha1($hashString, true));
    }

    public static function GUID()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf(
            '%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)
        );
    }

    public function get_client_ip()
    {
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = '127.0.0.1';
        }

        return $ipaddress;
    }
}
