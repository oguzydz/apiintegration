<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TrendyolService;
class TrendyolController extends Controller
{
    protected $client;

    public function __construct(TrendyolService $client)
    {
        $this->client = $client;
    }

    public function getProducts(Request $request)
    {
        return $this->client->getProducts($request->page, $request->size);
    }

    public function transferProducts(Request $request)
    {
        return $this->client->transferProducts($request->all());
    }

    public function updateProducts(Request $request)
    {
        return $this->client->updateProducts($request->all());
    }

    public function checkBatchRequest($id)
    {
        return $this->client->checkBatchRequest($id);
    }

    public function productCategories()
    {
        return $this->client->productCategories();
    }

    public function productCategoriesAttributes($categoryId)
    {
        return $this->client->productCategoriesAttributes($categoryId);
    }

    public function getBrands()
    {
        return $this->client->getBrands();
    }

    public function getShipmentProviders()
    {
        return $this->client->getShipmentProviders();
    }

    public function updatePriceAndInventory(Request $request)
    {
        return $this->client->updatePriceAndInventory($request->all());
    }

    /** Orders Starts */

    public function getOrders($orderByField, $orderByDirection, $size)
    {
        return $this->client->getOrders(
            $orderByField,
            $orderByDirection,
            $size
        );
    }

    public function updateTrackingNumber($shipmentPackageId, Request $request)
    {
        return $this->client->updateTrackingNumber(
            $shipmentPackageId,
            $request->trackingNumber
        );
    }

    public function updatePackageStatus($shipmentPackageId, Request $request)
    {
        return $this->client->updatePackageStatus(
            $shipmentPackageId,
            $request->all()
        );
    }

    public function setSupplierInvoiceLink(Request $request)
    {
        return $this->client->setSupplierInvoiceLink($request->all());
    }

    public function changeCargoProviders($shipmentPackageId, Request $request)
    {
        return $this->client->changeCargoProviders(
            $shipmentPackageId,
            $request->all()
        );
    }

    public function getClaims()
    {
        return $this->client->getClaims();
    }

    public function createClaim(Request $request)
    {
        return $this->client->createClaim($request->all());
    }

    public function approveClaim($claimId, Request $request)
    {
        return $this->client->approveClaim($request->all());
    }

    public function createRejection(
        $claimIssueReasonId,
        $claimItemIdList,
        $description
    ) {
        return $this->client->createRejection(
            $claimIssueReasonId,
            $claimItemIdList,
            $description
        );
    }

    public function getCustomerQuestions()
    {
        return $this->client->getCustomerQuestions();
    }

    public function getAddresses()
    {
        return $this->client->getAddresses();
    }

    public function answerCustomerQuestions($questionId, Request $request)
    {
        return $this->client->answerCustomerQuestions(
            $questionId,
            $request->all()
        );
    }
}
