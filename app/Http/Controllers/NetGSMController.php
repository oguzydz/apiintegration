<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class NetGSMController extends Controller
{
    public function accountDetail(Request $request)
    {
        $response = Http::get(
            'https://api.netgsm.com.tr/sms/header/',
            $request->all()
        );

        $body = $response->body();

        if ($body == 30 or $body == 40 or $body == 100) {
            $error = $this->errors($body);
            return [
                'error' => true,
                'msg' => $error,
            ];
        }

        $data = explode('<br>', $response);
        $data = array_filter($data);

        $responseData = [
            'user_code' => $data[0],
            'account_name' => $data[1],
        ];

        return $responseData;
    }

    public function sendSms(Request $request)
    {
        $url = 'https://api.netgsm.com.tr/sms/send/get/';
        $response = Http::get($url, $request->all());

        $body = $response->body();

        if ($body == 30 or $body == 40 or $body == 100) {
            $error = $this->errors($body);
            return [
                'error' => true,
                'msg' => $error,
            ];
        }

        $success = substr($body, 0, 2);

        if ($success == 00) {
            return [
                'error' => false,
                'msg' => 'SMS Başarıyla Gönderildi!',
            ];
        }
    }

    public function sendOtp(Request $request)
    {
        $url = 'https://api.netgsm.com.tr/sms/send/otp/';
        $response = Http::get($url, $request->all());

        $body = $response->body();

        if ($body == 30 or $body == 40 or $body == 100 or $body == 62) {
            $error = $this->errors($body);
            return [
                'error' => true,
                'msg' => $error,
            ];
        }


        $success = substr($body, 0, 2);

        if ($success == 00) {
            return [
                'error' => false,
                'msg' => 'SMS Başarıyla Gönderildi!',
            ];
        }
    }
    
    public function errors($errorCode)
    {
        $errors = [
            30 => 'Geçersiz kullanıcı adı , şifre veya kullanıcınızın API erişim izninin olmadığını gösterir. Ayrıca eğer API erişiminizde IP sınırlaması yaptıysanız ve sınırladığınız ip dışında gönderim sağlıyorsanız 30 hata kodunu alırsınız. API erişim izninizi veya IP sınırlamanızı , web arayüzümüzden; sağ üst köşede bulunan ayarlar> API işlemleri menüsunden kontrol edebilirsiniz.',
            40 => 'Arama kriterlerinize göre listelenecek kayıt olmadığını ifade eder.',
            100 => 'Sistem hatası, sınır aşımı.(dakikada en fazla 5 kez sorgulanabilir.)',
            62 => 'Hesabınızda OTP SMS Paketi tanımlı değildir, kontrol ediniz.'
        ];

        return $errors[$errorCode];
    }
}
