<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PaynetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $secret_key = 'sck_qAylgn45uWwmNhXtgQdTte9zArq-';
        $PostURL = 'https://pts-api.paynet.com.tr/v1/transaction/charge';

        $session_id = 0;
        $token_id = 'TEST';

        $params = [
            'session_id' => $session_id,
            'token_id' => $token_id,
            'transaction_type' => 1,
        ];

        $options = [
            'http' => [
                'header' =>
                    "Accept: application/json; charset=UTF-8\r\n" .
                    "Content-type: application/json; charset=UTF-8\r\n" .
                    'Authorization: Basic ' .
                    $secret_key,
                'method' => 'POST',
                'content' => json_encode($params),
                'ignore_errors' => true,
            ],
        ];

        // $context = stream_context_create($options);
        // $result = json_decode(@file_get_contents($PostURL, false, $context));

        // if ($result->is_succeed == true) {
        //     echo 'Başarılı';
        // } else {
        //     echo 'Başarısız';
        // }

        $url = "https://api.ipara.com/";


        $response = Http::withHeaders([
            'Authorization' => 'Basic sck_pcs_jECRyoZq0khnsV9elaF/SIBVvxQI+bIW',
        ])->post('https://api.paynet.com.tr/v1/transaction/charge');

        dd($response);
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
