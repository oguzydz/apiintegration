<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Services\YurticiKargoService;

class YurticiKargoController extends Controller
{
    /**
     * YurtIcı constructor.
     * @param null $username
     * @param null $password
     * @param string $language
     */
    public function __construct(Request $request)
    {
        $this->talepNo = $request->type;

        $this->accountSettings = [
            'sender' => [
                // Gönderici Ödemeli
                'username' => '1088N590350522G',
                'password' => '4nc9iBW9N73E5113',
                'language' => 'TR',
            ],
            'receiver' => [
                // Kapıda Ödemeli
                'username' => '1088T590350522G',
                'password' => 'n62u8DUxw9sM5De1',
                'language' => 'TR',
            ],
        ];

        $this->username = $this->accountSettings[$this->talepNo]['username'];
        $this->password = $this->accountSettings[$this->talepNo]['password'];
        $this->language = $this->accountSettings[$this->talepNo]['language'];

        $this->yurtici = new YurticiKargoService([
            'username' => $this->username,
            'password' => $this->password,
            'test' => false, //TEST MODE true / false
        ]);
    }

    public function createCargo(Request $request)
    {
        $yurtici = new YurticiKargoService([
            'username' => $this->username,
            'password' => $this->password,
            'test' => false, //TEST MODE true / false
        ]);

        $createCargo = $yurtici->createCargo($request->all());
        return ['response' => $createCargo];
    }

    public function cargoStatus(Request $request)
    {
        $yurtici = new YurticiKargoService([
            'username' => $this->username,
            'password' => $this->password,
            'test' => false, //TEST MODE true / false
        ]);

        $kargoDurum = $yurtici->cargoStatus($request->all());

        return ['response' => $kargoDurum];
    }

    public function cancelCargo(Request $request)
    {
        $yurtici = new YurticiKargoService([
            'username' => $this->username,
            'password' => $this->password,
            'test' => false, //TEST MODE true / false
        ]);

        $kargoIptal = $this->yurtici->cancelCargo($request->all());

        return ['response' => $kargoIptal];
    }
}
