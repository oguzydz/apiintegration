<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\N11Service;

class N11Controller extends Controller
{
    protected $apiKey;
    protected $apiSecret;
    protected $n11;

    public function __construct()
    {
        $this->apiKey = '2ff28d33-5c08-42cd-83d8-9deb6bcfa32d';
        $this->apiSecret = 'YhiXO3pEMOS043Rx';

        $client = new N11Service([
            'appKey' => $this->apiKey,
            'appSecret' => $this->apiSecret,
        ]);

        $this->n11 = $client;
    }

    public function getCities()
    {
        return $this->n11->GetCities();
    }
    public function getCategories()
    {
        return $this->n11->GetTopLevelCategories();
    }

    public function getProductList(Request $request)
    {
        return $this->n11->GetProductList(
            $request->itemsPerPage,
            $request->currentPage
        );
    }
    public function getProductBySeller(Request $request)
    {
        return $this->n11->GetProductBySellerCode($request->productSellerCode);
    }

    public function saveProduct(Request $request)
    {
        return $this->n11->SaveProduct($request->all());
    }

    public function deleteProductBySeller(Request $request)
    {
        return $this->n11->DeleteProductBySellerCode($request->productSellerCode);
    }

    public function orderList(Request $request)
    {
        return $this->n11->OrderList($request->searchData);
    }

    public function getShipmentTemplate(Request $request)
    {
        return $this->n11->GetShipmentTemplate($request->name);
    }
    public function getShipmentCompanies()
    {
        return $this->n11->GetShipmentCompanies();
    }
}
