<?php

namespace App\Http\Controllers;

use SoapClient;
use Illuminate\Http\Request;
use PharIo\Manifest\InvalidUrlException;

use App\Services\ArasKargoService;

class ArasKargoController extends Controller
{
    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function customerReference(
        $customerId,
        ArasKargoService $arasKargoService
    ) {
        $queryInfo = "<QueryInfo>
        <QueryType>1</QueryType>
        <IntegrationCode>$customerId</IntegrationCode>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function dateCargo($date, ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>2</QueryType>
        <Date>$date</Date>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

        /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function getCargoInformation($trackingNumber, ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>21</QueryType>
        <IntegrationCode>$trackingNumber</IntegrationCode>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }


    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function dueDateCargo($date, ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>3</QueryType>
        <Date>$date</Date>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function dueDateIrsaliye($date, ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>4</QueryType>
        <Date>$date</Date>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function waitingCargo(ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>5</QueryType>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function directedCargo($date, ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>6</QueryType>
        <Date>$date</Date>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function returnedCargo($date, ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>7</QueryType>
        <Date>$date</Date>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function sentBackCargo($date, ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>8</QueryType>
        <Date>$date</Date>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function movingInfoCargo(
        $cargoFollowingCode,
        ArasKargoService $arasKargoService
    ) {
        $queryInfo = "<QueryInfo>
        <QueryType>9</QueryType>
        <IntegrationCode>$cargoFollowingCode</IntegrationCode>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function allStoreData(ArasKargoService $arasKargoService)
    {
        $queryInfo = "<QueryInfo>
        <QueryType>10</QueryType>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }
    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function customerReferenceList(
        $customerIds,
        ArasKargoService $arasKargoService
    ) {
        $queryInfo = "<QueryInfo>
        <QueryType>11</QueryType>
        <IntegrationCode>$customerIds</IntegrationCode>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function intervalIrsaliye(
        $startDate,
        $endDate,
        ArasKargoService $arasKargoService
    ) {
        $queryInfo = "<QueryInfo>
        <QueryType>12</QueryType>
        <StartDate>$startDate</StartDate>
        <EndDate>$endDate</EndDate>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }
    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function intervalIrsaliyeWithPayment(
        $startDate,
        $endDate,
        ArasKargoService $arasKargoService
    ) {
        $queryInfo = "<QueryInfo>
        <QueryType>13</QueryType>
        <StartDate>$startDate</StartDate>
        <EndDate>$endDate</EndDate>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function cargoBetweenDate(
        $startDate,
        $endDate,
        ArasKargoService $arasKargoService
    ) {
        $queryInfo = "<QueryInfo>
        <QueryType>16</QueryType>
        <StartDate>$startDate</StartDate>
        <EndDate>$endDate</EndDate>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }

    /**
     * Display a listing of the Customer Cargo.
     *
     * @return \Illuminate\Http\Response
     */

    public function invoiceInfo(
        $reportType,
        $serialNumber,
        ArasKargoService $arasKargoService
    ) {
        $queryInfo = "<QueryInfo>
        <QueryType>22</QueryType>
        <ReportType>$reportType</ReportType>
        <InvoiceSerialNumber>$serialNumber</InvoiceSerialNumber>
        </QueryInfo>";

        $client = $arasKargoService->ArasClient($queryInfo);

        return $client;
    }
}
